<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */
    'MenuMgr' => Tecpro\Core\Scripts\Managers\Facades\MenuMgr::class,
    'FilterMgr' => Tecpro\Core\Scripts\Managers\Facades\FilterMgr::class,
    'PageMetadataMgr' => Tecpro\Core\Scripts\Managers\Facades\PageMetadataMgr::class,

];
