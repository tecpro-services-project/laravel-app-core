<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin configuration
    |--------------------------------------------------------------------------
    |
    */
    'locale' => 'en',
    'group' => [
        'name' => 'admin.',
        'prefix' => 'admin'
    ],
    'namespace' => [
        'view' => 'admin',
        'lang' => 'core'
    ],
    'forms' => [
        'default' => [
            'fields' => [
                'exclude' => [
                    'created_at', 'updated_at'
                ],
                'hidden' => [
                    'locale_id'
                ],
                'force_type' => [
                    'product::image' => 'file',
                    'instructor::avatar' => 'file'
                ]
            ]
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | CMS Admin configuration
    |--------------------------------------------------------------------------
    |
    */
    'menu' => [
        menu('', 'Application', [
            menu('', 'Contents', [
                menu('/admin/cms/category/list', 'Category', null),
                menu('/admin/cms/asset/list', 'Asset', null),
                menu('/admin/cms/slot/list', 'Slot', null),
            ]),
            menu('', 'Products', [
                menu('/admin/ecom/product/list', 'Product', null),
                menu('/admin/ecom/category/list', 'Category', null),
            ]),
        ])
    ]
];
