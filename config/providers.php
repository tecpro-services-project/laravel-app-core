<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication providers configuration
    |--------------------------------------------------------------------------
    |
    */
    'admin' => [
        'driver' => 'eloquent',
        'model' => Tecpro\Core\App\Models\Admin::class,
    ]
];