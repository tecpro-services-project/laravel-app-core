// import plugins
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const clientBaseDir = './src/resources/';
const exportProcess = [];

const bootstrapPackages = {
    Alert: 'exports-loader?Alert!bootstrap/js/src/alert',
    // Button: 'exports-loader?Button!bootstrap/js/src/button',
    Carousel: 'exports-loader?Carousel!bootstrap/js/src/carousel',
    Collapse: 'exports-loader?Collapse!bootstrap/js/src/collapse',
    // Dropdown: 'exports-loader?Dropdown!bootstrap/js/src/dropdown',
    Modal: 'exports-loader?Modal!bootstrap/js/src/modal',
    // Popover: 'exports-loader?Popover!bootstrap/js/src/popover',
    Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/src/scrollspy',
    Tab: 'exports-loader?Tab!bootstrap/js/src/tab',
    // Tooltip: 'exports-loader?Tooltip!bootstrap/js/src/tooltip',
    Util: 'exports-loader?Util!bootstrap/js/src/util'
};

const webpackHelpers = {
    generateResourceFilePath: (type) => {
        var resourceFileDir = path.join(clientBaseDir, type);
        var entryObjects = {};

        if (!fs.existsSync(resourceFileDir)) {
            console.warn(`${resourceFileDir} does not exist.`);
            return [];
        }

        fs.readdirSync(resourceFileDir)
            .filter(file => {
                return new RegExp(`.${type}$`, 'g').test(file);
            })
            .forEach(file => {
                var fileName = file.replace(new RegExp(`.${type}$`, 'g'), '');
                entryObjects[fileName] = '.\\' + path.join(resourceFileDir, file);
            });

        return entryObjects;
    }
}

const scssConfig = {
    name: 'scss',
    entry: webpackHelpers.generateResourceFilePath('scss'),
    output: {
        filename: "[name].css",
        path: path.resolve(__dirname, "src/public/css")
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name].css'
        }),
        new OptimizeCSSAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
                preset: ['default', { discardComments: { removeAll: true } }]
            },
            canPrint: true
        })
    ],
    module: {
        rules: [
            {
                test: /\.(sa|sc)ss$/,
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            // Ignore all the url() attribute in scss files
                            url: false
                        }
                    }, {
                        loader: 'sass-loader'
                    }]
                })
            }
        ]
    }
};

const jsConfig = {
    name: 'js',
    entry: webpackHelpers.generateResourceFilePath('js'),
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "src/public/js")
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            jquery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            ...bootstrapPackages
        })
    ]
};

module.exports = env => {
    const mode = env.mode ? env.mode : 'production';
    const target = (env.target === 'scss' || env.target === 'js') ? env.target : 'both';

    switch (target) {
        case 'scss':
            exportProcess.push(scssConfig);
            break;
    
        case 'js':
            exportProcess.push(jsConfig);
            break;
    
        default:
            exportProcess.push(scssConfig, jsConfig);
            break;
    }

    return exportProcess.map(proc => {
        proc.mode = mode;
        return proc;
    });
};