<?php

use Illuminate\Support\Facades\Route;
use Tecpro\Core\App\Http\Controllers\AdminLoginController;
use Tecpro\Core\App\Http\Controllers\AdminDashboardController;
use Tecpro\Core\Scripts\Support\Form\FormTable;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
| All request will have the prefix /admin/...
|
*/

Route::middleware('guest')->group(function () {
    Route::get('/login', [AdminLoginController::class, 'show'])->name('login');
    Route::post('/login', [AdminLoginController::class, 'login'])->name('submit.login');
});

/**
 * Only authenticated account can access these route
 */
Route::middleware('coreAuth:admin')->group(function () {
    Route::get('/', [AdminDashboardController::class, 'show'])->name('dashboard');
    Route::get('/logout', [AdminLoginController::class, 'logout'])->name('logout');
});

Route::get('/test', function () {
    $formTable = new FormTable(['product', 'product_detail'], 'POST', '', config('admin.forms.default'));

    return view('admin::tests.formTest', [
        'formTable' => $formTable
    ]);
});
