<?php

namespace Tecpro\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;

class CoreSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admins = [[
                'first_name' => 'Dat',
                'last_name' => 'Kieu',
                'email' => 'dat.kieu@tecproservices.net',
                'password' => Hash::make('123456'),
            ],
            [
                'first_name' => 'Xuan',
                'last_name' => 'Vo',
                'email' => 'xuan.vo@tecproservices.net',
                'password' => Hash::make('123456'),
            ],
            [
                'first_name' => 'Tam',
                'last_name' => 'Nguyen',
                'email' => 'tam.nguyen@tecproservices.net',
                'password' => Hash::make('123456'),
            ],
            [
                'first_name' => 'Duc',
                'last_name' => 'Hoang',
                'email' => 'duc.hoang@tecproservices.net',
                'password' => Hash::make('123456'),
            ],
            [
                'first_name' => 'Dai',
                'last_name' => 'Phan',
                'email' => 'dai.phan@tecproservices.net',
                'password' => Hash::make('123456'),
            ],
            [
                'first_name' => 'Khoi',
                'last_name' => 'Pham',
                'email' => 'khoi.pham@tecproservices.net',
                'password' => Hash::make('123456'),
            ]];
        
        $now = Date::now();

        for ($idx = 0; $idx < count($admins); $idx++) { 
            $admins[$idx]['phone'] = '999';
            $admins[$idx]['created_at'] = $now;
            $admins[$idx]['updated_at'] = $now;
        }

        \Tecpro\Core\App\Models\Admin::insert($admins);
    }
}
