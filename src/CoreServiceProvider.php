<?php
namespace Tecpro\Core;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Tecpro\Core\App\Http\Middleware\AdminAuthenticate;
use Tecpro\Core\Scripts\Managers\Facades\MenuMgr;

class CoreServiceProvider extends ServiceProvider {

    private $group;

    public function boot() {
        $this->group = config('admin.group');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadRoutes();
        $this->loadViewsFrom(__DIR__.'/resources/views', config('admin.namespace.view'));
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', config('admin.namespace.lang'));
        $this->loadAsset();
        $this->loadCommand();
        $this->loadMiddleware();
        $this->loadSingletonFacade();

        // Check the request if it matches the route with name admin.*
        if (preg_match("(\/admin)", Request::getRequestUri()) === 1) {
            $this->loadAdminViewData();
        } else {
            $this->loadViewData();
        }
    }

    public function register() {
        $this->loadAdminConfiguration();
    }

    /**
     * Load commands
     */
    protected function loadCommand() {
        if ($this->app->runningInConsole()) {
            // Registering package commands.
            $this->commands([
                \Tecpro\Core\App\Console\Commands\CreateViewDataCommand::class,
                \Tecpro\Core\App\Console\Commands\MakeFormConfigCommand::class,
                \Tecpro\Core\App\Console\Commands\ExportExtensionConfiguration::class,
            ]);
        }
    }

    /**
     * Publish the asset to the folder /public of the main app
     */
    protected function loadAsset() {
        // Example: Copied Directory [\packages\laravel-app-core\src\public\theme\css] To [\public\theme\css]
        // php artisan vendor:publish --provider="Tecpro\Core\CoreServiceProvider" --tag=client
        $this->publishes([
            __DIR__.'/public/css' => public_path('vendor/laravel-app-core/css'),
            __DIR__.'/public/js' => public_path('vendor/laravel-app-core/js'),
            __DIR__.'/public/theme/css' => public_path('vendor/laravel-app-core/theme/css'),
            __DIR__.'/public/theme/js' => public_path('vendor/laravel-app-core/theme/js'),
        ], 'client');

        // php artisan vendor:publish --provider="Tecpro\Core\CoreServiceProvider" --tag=static
        $this->publishes([
            __DIR__.'/public/theme/img' => public_path('vendor/laravel-app-core/theme/img'),
            __DIR__.'/public/static' => public_path('vendor/laravel-app-core/static'),
        ], 'static');
    }

    /**
     * Load all routes for administrator
     */
    protected function loadRoutes() {
        // Add prefix /admin for all URL
        Route::name($this->group['name'])
            ->prefix($this->group['prefix'])
            ->middleware('web')
            ->group(function () {
                $this->loadRoutesFrom(__DIR__.'/routes/core.php');
            });
    }

    /**
     * Load the view data from the main app
     */
    protected function loadViewData() {
        /**
         * Using view composer to set following variables globally
         * 
         * @param \Illuminate\View\View $view
         */
        view()->composer('*', function($view) {
            $authUser = Auth::user();
            $viewData = [
                'authUser' => $authUser
            ];

            if (class_exists(\App\ViewData\ViewDataLoader::class)) {
                foreach (\App\ViewData\ViewDataLoader::$loaders as $loader) {
                    $loaderObj = new $loader;
                    $loaderObj->setAuthUser($authUser);
                    $viewData[$loaderObj->getName()] = $loaderObj->dump();
                }
            }

            $view->with('viewData', $viewData);
        });
    }

    /**
     * Load view data for only admin
     */
    protected function loadAdminViewData() {
        view()->composer('*', function($view) {
            $authAdmin = Auth::user();
            $adminViewData = [
                'authAdmin' => $authAdmin,
                'menu' => config('admin.menu')
            ];

            $view->with('adminViewData', $adminViewData);
        });
    }

    /**
     * Load admin configuration
     */
    protected function loadAdminConfiguration() {
        $this->mergeConfigFrom(
            __DIR__.'/../config/admin.php', 'admin'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/guards.php', 'auth.guards'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/providers.php', 'auth.providers'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/aliases.php', 'app.aliases'
        );
    }

    /**
     * Load middleware
     */
    protected function loadMiddleware() {
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('coreAuth', AdminAuthenticate::class);
    }

    /**
     * Load App singleton facade
     */
    public function loadSingletonFacade() {
        $this->app->singleton('menuMgr', function () {
            return new \Tecpro\Core\Scripts\Managers\MenuMgr();
        });

        $this->app->singleton('pageMetadataMgr', function () {
            return new \Tecpro\Core\Scripts\Managers\PageMetadataMgr();
        });
    }
}
