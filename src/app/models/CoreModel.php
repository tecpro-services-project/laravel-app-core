<?php

namespace Tecpro\Core\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

abstract class CoreModel extends Model
{
    /**
     * @var bool $isAllowUpdateKey Allow to update the primary key of the model
     */
    protected $isAllowUpdateKey = false;

    /**
     * Return the value that allows to update the primary key of the model
     * @return bool The value that allows to update the primary key of the model
     */
    public function getIsAllowUpdateKey() {
        return $this->isAllowUpdateKey;
    }

    /**
     * Assign the created_at and updated_at timestamp if the record does not contain them
     * @param array $record The record
     * @return array The record has created_at and updated_at timestamp 
     */
    public function createTimeStampIfNotExist(array $record) {
        $now = Date::now();
        $record['created_at'] = !isset($record['created_at']) ? $now : $record['created_at'];
        $record['updated_at'] = !isset($record['updated_at']) ? $now : $record['updated_at'];

        return $record;
    }

    /**
     * Filter the provided data to the have only the fillable data
     * @param array $raw The raw data array
     * @return array The filtered data array
     */
    public function filter(array $raw = [], bool $isAllowUpdateKey = false) {
        $filtered = [];

        foreach ($this->fillable as $column) {
            // If system update data to database, the table's ID will not be updated
            if (strcmp($column, $this->getKeyName()) === 0 && !$isAllowUpdateKey) continue;

            $filtered[$column] = isset($raw[$column]) ? $raw[$column] : null;
        }

        return $filtered;
    }

    /**
     * Filter the array parameter to get only the data that belongs to the fillable columns and update it to database
     * @param array $record The record array data
     * @return $this
     */
    public function filterUpdate(array $record) {
        // Re-use the timestamp created_at
        $record['created_at'] = $this->created_at;
        $record = $this->createTimeStampIfNotExist($record);
        $filtered = $this->filter($record, $this->isAllowUpdateKey);
        $this->update($filtered);

        return $this;
    }

    /**
     * Filter the array parameter to get only the data that belongs to the fillable columns and create it to database
     * @param array $record The record array data
     * @return $this
     */
    public function filterCreate(array $record) {
        $record = $this->createTimeStampIfNotExist($record);
        $filtered = $this->filter($record, true);
        $this->create($filtered);

        return $this;
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    abstract public function transform(string $localeId = '');
}
