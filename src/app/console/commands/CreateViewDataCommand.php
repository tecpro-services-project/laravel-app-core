<?php

namespace Tecpro\Core\App\Console\Commands;

use Error;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CreateViewDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:viewdata {viewDataName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new view data class in folder app/ViewData';

    /**
     * The View Data app path
     *
     * @var string
     */
    protected $viewDataAppPath;

    /**
     * The View Data Loader name
     *
     * @var string
     */
    protected $viewDataLoader;

    /**
     * The View Data Loader path
     *
     * @var string
     */
    protected $viewDataLoaderPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->viewDataAppPath = app_path('ViewData');
        $this->viewDataLoader = 'ViewDataLoader';
        $this->viewDataLoaderPath = makePath($this->viewDataAppPath . '/' . $this->viewDataLoader . '.php', true);
    }

    public function installViewDataLoader()
    {
        if (!File::exists($this->viewDataAppPath)) {
            File::makeDirectory($this->viewDataAppPath);
        }

        if (!File::exists($this->viewDataLoaderPath)) {
            $phpScript = 
        '<?php

namespace App\ViewData;

class ViewDataLoader {
    /**
     * Setup the ViewData object in this array. They will be loaded into view {{ $viewData[<Your view data name>][Your view data attribute] }}
     */
    public static $loaders = [
        // ViewData::class
    ];
}
        ';

            File::put($this->viewDataLoaderPath, $phpScript);
        }
    }

    public function createViewDataClass() {
        $viewDataNameArg = $this->argument('viewDataName');
        $viewDataName = Str::camel($viewDataNameArg);
        $viewDataClass = Str::ucfirst($viewDataNameArg);

        if (strcmp($viewDataClass, 'ViewDataSample') === 0) {
            throw new Error('Can not create the view data class with the provided name because the file is the sample file');
        }

        $sampleFilePath = __DIR__ . '/../../../scripts/support/viewData/ViewDataSample.php';
        $sampleFileScript = File::get($sampleFilePath);

        // Replace the class name
        $sampleFileScript = preg_replace(
            '/(ViewDataSample)/',
            $viewDataClass,
            $sampleFileScript
        );

        // Replace the namespace
        $sampleFileScript = preg_replace(
            '/(Tecpro\\\Core\\\Scripts\\\Support\\\ViewData)/',
            'App\\\ViewData',
            $sampleFileScript
        );

        // Replace the use link ViewData
        $sampleFileScript = preg_replace(
            '/(\#{{useViewData}})/',
            'use Tecpro\\\Core\\\Scripts\\\Support\\\ViewData\\\ViewData;',
            $sampleFileScript
        );

        // Replace the sample view data name to be called in view
        $sampleFileScript = preg_replace("/(viewDataName)/i", $viewDataName, $sampleFileScript);

        File::put($this->viewDataAppPath . '/' . $viewDataClass . '.php', $sampleFileScript);

        $this->info('Create new view data class with namespace App\\ViewData\\' . $viewDataClass);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->installViewDataLoader();

        $this->createViewDataClass();     

        return 0;
    }
}
