<?php

namespace Tecpro\Core\App\Console\Commands;

use Illuminate\Console\Command;
use Tecpro\Core\Scripts\Support\Extension\ExtensionFactory;

class ExportExtensionConfiguration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extension:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the final extension configuration to app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $final = ExtensionFactory::exportFinalConfiguration();

        dump($final);

        return 0;
    }
}
