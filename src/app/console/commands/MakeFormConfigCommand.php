<?php

namespace Tecpro\Core\App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MakeFormConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:tableform {tableName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new form config from the provided tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tableName = $this->argument('tableName');
        $exclude = ['created_at', 'updated_at'];
        $describe = DB::select('DESCRIBE ' . $tableName);
        $formFieldConfigs = [];
        $formValidationConfigs = [];
        
        foreach ($describe as $column) {
            $field = $column->Field;
            $type = $column->Type;
            $isNull = $column->Null;
            $validate = [];
            $formFieldConfig = ['id' => $field];

            if (array_search($field, $exclude) !== false) {
                continue;
            }

            if (strcmp($isNull, 'NO') === 0) {
                array_push($validate, 'required');
            }

            // varchar: text
            preg_match('/(varchar)\((\d+)\)/', $type, $typeMatch);

            if (count($typeMatch) !== 0) {
                array_push($validate, 'max:' . $typeMatch[2]);
                $formFieldConfig['type'] = 'text';
            }

            // text: textarea
            preg_match('/(text)/', $type, $typeMatch);

            if (count($typeMatch) !== 0) {
                $formFieldConfig['type'] = 'textarea';

                switch ($type) {
                    case 'tinytext':
                        array_push($validate, 'max:255');
                        break;

                    case 'text':
                        array_push($validate, 'max:65535');
                        break;
                    
                    case 'mediumtext':
                        array_push($validate, 'max:16777215');
                        break;
                    
                    case 'longtext':
                        array_push($validate, 'max:4294967295');
                        break;
                }
            }

            // integer: number
            preg_match('/(int)/', $type, $typeMatch);

            if (count($typeMatch) !== 0) {
                $formFieldConfig['type'] = 'number';
            }

            // tinyint: checkbox
            preg_match('/(tinyint)/', $type, $typeMatch);

            if (count($typeMatch) !== 0) {
                $formFieldConfig['type'] = 'checkbox';
            }

            $formValidationConfigs[$field] = implode('|', $validate);

            array_push($formFieldConfigs, $formFieldConfig);
        }

        foreach ($formValidationConfigs as $field => $formValidation) {
            $this->info("'$field' => '$formValidation',");
        }

        return 0;
    }
}
