<?php

namespace Tecpro\Core\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function show() {
        return view('admin::login');
    }

    public function login(Request $request) {
        $credentials = $request->only(['email', 'password']);
    
        /**
         * Auth::guard()->attempt($credentials) will validate and create the session if the validation is successfull.
         */
        return Auth::guard('admin')->attempt($credentials) ? redirect()->route('admin.dashboard') : redirect()->back()->withErrors([
            'message' => __('auth.failed')
        ]);
    }

    public function logout(Request $request) {
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('admin.login');
    }
}
