<?php

namespace Tecpro\Core\App\Http\Controllers;

use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
    public function show() {
        return view('admin::dashboard');
    }
}
