<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error message
    |--------------------------------------------------------------------------
    |
    */
    'form.error.tables.type.incorrect' => 'FormTable can not recognize the table name array parameter'
];
