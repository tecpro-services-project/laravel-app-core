<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email or password is incorrect',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    /*
    |--------------------------------------------------------------------------
    | Login page resources
    |--------------------------------------------------------------------------
    */
    'login.form.title' => 'Admin Instance',
    'login.form.email.label' => 'Email address',
    'login.form.email.placeholder' => '',
    'login.form.password.label' => 'Password',
    'login.form.password.placeholder' => '',
    'login.form.submit' => 'Sign in'

];
