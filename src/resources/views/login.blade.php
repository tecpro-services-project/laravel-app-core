@extends('admin::layouts.auth')
@section('title', 'TECPRO SERVICES ADMIN')

@section('asset')
<link href="{{ asset('vendor/laravel-app-core/css/login.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="page page-center">
        <div class="container container-tight py-4">
            <div class="text-center mb-4">
                <img src="{{ asset('vendor/laravel-app-core/static/logo/logo_tps.jpg') }}" class="logo" alt="">
            </div>
            <div class="card card-md">
                <div class="card-body">
                    <h2 class="h2 text-center mb-4">{{ __('core::auth.login.form.title') }}</h2>
                    <form action="{{ route('admin.submit.login') }}" method="post" autocomplete="off" novalidate>
                        @csrf

                        <div class="mb-3">
                            <label class="form-label">{{ __('core::auth.login.form.email.label') }}</label>
                            <input type="email" name="email" class="form-control" placeholder="{{ __('core::auth.login.form.email.placeholder') }}"
                                autocomplete="off">
                        </div>
                        <div class="mb-2">
                            <label class="form-label">
                                {{ __('core::auth.login.form.password.label') }}
                            </label>
                            {{-- input-group-flat --}}
                            <div class="input-group">
                                <input type="password" name="password" class="form-control" placeholder="{{ __('core::auth.login.form.password.placeholder') }}"
                                    autocomplete="off">
                                <span class="input-group-text">
                                    <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip">
                                        <!-- Download SVG icon from http://tabler-icons.io/i/eye -->
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                            viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <circle cx="12" cy="12" r="2" />
                                            <path
                                                d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" />
                                        </svg>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary w-100">{{ __('core::auth.login.form.submit') }}</button>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-important alert-danger alert-dismissible mt-3" role="alert">
                                <div class="d-flex">
                                    <div>
                                        <!-- Download SVG icon from http://tabler-icons.io/i/alert-circle -->
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24"
                                            height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <circle cx="12" cy="12" r="9" />
                                            <line x1="12" y1="8" x2="12" y2="12" />
                                            <line x1="12" y1="16" x2="12.01" y2="16" />
                                        </svg>
                                    </div>
                                    <div>
                                        {{ $errors->first() }}
                                    </div>
                                </div>
                                <a class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="close"></a>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
