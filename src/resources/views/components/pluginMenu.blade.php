@isset($adminViewData)
    @isset($adminViewData['menu'])

        @foreach ($adminViewData['menu'] as $menu1stLevel)
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside"
                    role="button" aria-expanded="false">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <polyline points="12 3 20 7.5 20 16.5 12 21 4 16.5 4 7.5 12 3" />
                            <line x1="12" y1="12" x2="20" y2="7.5" />
                            <line x1="12" y1="12" x2="12" y2="21" />
                            <line x1="12" y1="12" x2="4" y2="7.5" />
                            <line x1="16" y1="5.25" x2="8" y2="9.75" />
                        </svg>
                    </span>
                    <span class="nav-link-title">
                        {{ $menu1stLevel['name'] }}
                    </span>
                </a>
                <div class="dropdown-menu">

                    @isset($menu1stLevel['sub'])
                        @foreach ($menu1stLevel['sub'] as $menu2ndLevel)
                            <div class="dropdown-menu-columns">
                                <div class="dropdown-menu-column">
                                    <div class="dropend">
                                        <a class="dropdown-item dropdown-toggle" href="#sidebar-cards" data-bs-toggle="dropdown"
                                            data-bs-auto-close="outside" role="button" aria-expanded="false">
                                            {{ $menu2ndLevel['name'] }}
                                        </a>

                                        @isset($menu2ndLevel['sub'])
                                            <div class="dropdown-menu">
                                                @foreach ($menu2ndLevel['sub'] as $menu3rdLevel)
                                                    <a href="{{ $menu3rdLevel['url'] }}" class="dropdown-item">
                                                        {{ $menu3rdLevel['name'] }}
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endisset

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset

                </div>
            </li>
        @endforeach

    @endisset
@endisset
