<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>@yield('title', 'TECPRO SERVICES')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('vendor/laravel-app-core/static/logo/logo_tps_favicon.jpg') }}">
    <!-- CSS files -->
    <link href="{{ asset('vendor/laravel-app-core/theme/css/tabler.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/laravel-app-core/theme/css/tabler-flags.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/laravel-app-core/theme/css/tabler-payments.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/laravel-app-core/theme/css/tabler-vendors.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/laravel-app-core/theme/css/demo.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/laravel-app-core/css/global.css') }}" rel="stylesheet" />
    <style>
        @import url('https://rsms.me/inter/inter.css');

        :root {
            --tblr-font-sans-serif: Inter, -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
        }
    </style>

    @yield('asset', '')

    <script src="{{ asset('vendor/laravel-app-core/theme/js/tabler.min.js') }}" defer></script>
    <script src="{{ asset('vendor/laravel-app-core/theme/js/demo.min.js') }}" defer></script>
    <script src="{{ asset('vendor/laravel-app-core/theme/js/demo-theme.min.js') }}" defer></script>
    <script src="{{ asset('vendor/laravel-app-core/js/main.js') }}" defer></script>

</head>

<body>
    
    @yield('content')

</body>

</html>
