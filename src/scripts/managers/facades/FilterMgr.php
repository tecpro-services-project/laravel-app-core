<?php

namespace Tecpro\Core\Scripts\Managers\Facades;

use Illuminate\Support\Facades\Facade;

class FilterMgr extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'filterMgr';
    }
}