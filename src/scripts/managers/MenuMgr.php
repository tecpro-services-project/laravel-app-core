<?php

namespace Tecpro\Core\Scripts\Managers;

use Error;

class MenuMgr {
    /**
     * @var array The menu configuration
     */
    protected $menu = [];

    /**
     * Require and merge the menu configuration
     * @param string $path The file path
     */
    public function merge(string $path) {
        $menuConfig = require($path);

        if (!isset($menuConfig) || gettype($menuConfig) !== 'array') {
            throw new Error('Menu configuration should return the array');
        }

        array_merge($this->menu, $menuConfig);
    }

    /**
     * Dump the menu data. TODO: Enhance to store the data in table
     * @return array The menu data array
     */
    public function dump() {
        $menu = config('app.menu');
        return isset($menu) && gettype($menu) === 'array' ? $menu : [];
    }
}