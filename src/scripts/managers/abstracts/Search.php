<?php

namespace Tecpro\Core\Scripts\Managers\Abstracts;

use Error;

abstract class Search
{
    /**
     * @var string
     */
    protected $filter;

    /**
     * @var string
     */
    protected $filterRules;

    /**
     * @var string
     */
    protected $sorting;

    /**
     * @var array
     */
    protected $sortingRules;

    /**
     * @var \Illuminate\Database\Eloquent\Collection
     */
    protected $searchResultCollection;

    public function __construct()
    {
        $this->sortingRules = [
            'price-low-to-high' => ['price', 'asc'],
            'price-high-to-low' => ['price', 'desc'],
            'oldest' => ['created_at', 'asc'],
            'newest' => ['created_at', 'desc'],
        ];
    }

    /**
     * Use filter rule and sorting rule to search for the records
     */
    abstract public function search();

    /**
     * Set the filter rule to the current search
     * @param string $filter The filter rule
     * @return \Search
     */
    public function setFilter(string $filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * Set the sorting rule ID to the current search
     * @param string $sorting The sorting ID
     * @return \Search
     */
    public function setSorting(string $sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * Get the filter rule
     * @return string The filter rule
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Get the sorting rule
     * @return string The sorting rule
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Get the search result collection
     * @return \Illuminate\Database\Eloquent\Collection Search result collection
     */
    public function getSearchResultCollection() {
        return $this->searchResultCollection;
    }

    /**
     * Get the sorting rule by ID
     * @param string $sortingId The sorting ID
     * @return array The sorting rule
     */
    public function getSortingRuleById(string $sortingId)
    {
        if (!isset($this->sortingRules[$sortingId])) {
            throw new Error('Sorting rule ID "' . $sortingId . '" does not exist.');
        }

        return $this->sortingRules[$sortingId];
    }
}
