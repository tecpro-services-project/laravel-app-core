<?php

namespace Tecpro\Core\Scripts\Managers\Abstracts;

use Tecpro\Core\Scripts\Managers\Traits\HasEnable;

abstract class DefaultMgr {

    use HasEnable;
    /**
     * Get data action
     */
    abstract public function get(string $recordId);

    /**
     * Get data action
     */
    abstract public function getMultiple(array $recordIds);

    /**
     * Create data action
     */
    abstract public function create(array $record);

    /**
     * Update data action
     */
    abstract public function update(string $recordId, array $record);

    /**
     * Delete data action
     */
    abstract public function delete(string $recordId);
}