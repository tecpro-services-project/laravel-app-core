<?php

namespace Tecpro\Core\Scripts\Managers;

use Error;

class PageMetadataMgr {
    /**
     * @var array The menu configuration
     */
    protected $metadata = [];

    /**
     * Require and merge the menu configuration
     * @param string $path The file path
     */
    public function merge(string $path) {
        $requireMetadata = require($path);

        if (!isset($requireMetadata) || gettype($requireMetadata) !== 'array') {
            throw new Error('The metadata configuration should return the array');
        }

        // array_merge($this->metadata, $requireMetadata);

        foreach ($requireMetadata as $routeName => $data) {
            $this->metadata[$routeName] = (object) $data;
        }
    }

    /**
     * Get the page metadata based on provided route name
     * @param string $routeName The route name
     * @return object The page metadata casted object
     */
    public function get(string $routeName) {
        // if ()

        return $metadata[$routeName] ?? null;
    }
}