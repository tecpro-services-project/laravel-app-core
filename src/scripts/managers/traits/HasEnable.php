<?php

namespace Tecpro\Core\Scripts\Managers\Traits;

trait HasEnable {
    /**
     * Determine the given record is enable or not
     * @param array The content asset record
     * @return int Return 1: true, 0: false
     */
    public function isEnable(array $content) {
        return isset($content['is_enable'])
            && (
                $content['is_enable'] === 1
                || $content['is_enable'] === true
                || (gettype($content['is_enable']) === 'string' && (strcmp($content['is_enable'], 'on') === 0 || strcmp($content['is_enable'], '1') === 0))
            ) ? 1 : 0;
    }
}