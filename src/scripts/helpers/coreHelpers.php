<?php

use Illuminate\Support\Facades\Blade;
use Tecpro\Core\Scripts\Support\System\System;

if (!function_exists('menu')) {
    /**
     * Menu configuration
     * 
     * @param string $url The URl
     * @param string $name The name
     * @param array|null $sub The sub-name
     * @return array Return menu configuration
     */
    function menu(string $path, string $name, ?array $sub)
    {
        return [
            'path' => $path,
            'url' => url($path),
            'name' => $name,
            'sub' => $sub
        ];
    }
}

if (!function_exists('makePath')) {
    /**
     * Add trailing slashes to path
     * 
     * @param string $path The path to parse
     * @param bool $isFile Default is false. Set true will trip the right slash if the path is a file
     * @return string The path with trailing slashes.
     */
    function makePath(string $path = '', bool $isFile = false)
    {
        $clonePath = trim($path);

        if (strcmp($clonePath, '') === 0) return '';

        $clonePath = rtrim($clonePath, '/') . '/';
        $clonePath = '/' . ltrim($clonePath, '/');
        $clonePath = str_replace('\\', '/', $clonePath);
        $clonePath = preg_replace('/(\/){2,}/', '/', $clonePath);

        if (System::getOS() === System::OS_WIN) {
            $clonePath = ltrim($clonePath, '/');
        }

        if ($isFile) {
            $clonePath = rtrim($clonePath, '/');
        }

        return $clonePath;
    }
}

if (!function_exists('_echo')) {
    /**
     * Check isset and not null before echoing
     * @param string $data The data string
     */
    function _echo(?string $data)
    {
        echo $data ?? '';
    }
}

if (!function_exists('getPageMetadata')) {
    /**
     * Menu configuration
     * 
     * @param string $url The URl
     * @param string $name The name
     * @param array|null $sub The sub-name
     * @return array Return menu configuration
     */
    function getPageMetadata()
    {
        return [];
    }
}

if (!function_exists('scanDirectoryRecursive')) {
    /**
     * Scan and get all files and folder name in the directory and its sub directory recursively
     * 
     * @param string $path The path to scan
     * @return string[] The array of file path
     */
    function scanDirectoryRecursive(string $path)
    {
        $fileIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        $fileList = [];

        foreach ($fileIterator as $fileObject) {
            $filePath = $fileObject->getRealPath();
            if (!is_dir($filePath)) {
                array_push($fileList, $filePath);
            }
        }

        return $fileList;
    }
}

if (!function_exists('renderCustomPagination')) {
    /**
     * Render custom package pagination with Blade compile string function
     * 
     * @param array $data The data to be rendered
     * @return string The rendered HTML pagination string
     */
    function renderCustomPagination(array $data)
    {
        // Blade::compileString()
    }
}
