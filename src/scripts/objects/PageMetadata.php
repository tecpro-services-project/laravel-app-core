<?php

namespace Tecpro\Core\Scripts\Objects;

class PageMetadata {
    public $title;
    public $description;
    public $keyword;

    public function __construct($title, $description, $keyword) {
        $this->title = $title;
        $this->description = $description;
        $this->keyword = $keyword;
    }
}
