<?php

namespace Tecpro\Core\Scripts\Objects;

abstract class CoreObject {
    /**
     * Return the associative array contains object properties
     * @return array The associative array contains object properties
     */
    public function toArray()
    {
        $retArr = [];

        foreach ($this as $prop => $value) {
            $retArr[$prop] = $value;
        }

        return $retArr;
    }
}
