<?php

namespace Tecpro\Core\Scripts\Support\Form;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Form object only contains its elements, data
 */
class FormTable extends Form
{
    /**
     * @var FormTableElement[] The form elements
     */
    protected array $elements;

    /**
     * @var string[] $tables The form table name array
     */
    protected array $tables;

    /**
     * @var array $config The form table configuration
     */
    protected array $config;

    /**
     * Initialize the form with tables from database
     */
    public function __construct()
    {
        parent::__construct();

        $this->tables = [];
        $this->config = [];
    }

    /**
     * Get the form table name array
     * @return array The form table name array
     */
    public function getTables(){
        return $this->tables;
    }

    /**
     * Set the form table name array
     * @param string[] $tables The table name array
     */
    public function setTables(array $tables){
        $this->tables = $tables;
    }

    /**
     * Get the form table configuration
     * @return array The form table configuration
     */
    public function getConfig(){
        return $this->config;
    }

    /**
     * Set the form table configuration
     * @param array $config The form table configuration
     */
    public function setConfig(array $config){
        $this->config = $config;
    }

    /**
     * Append the tables name
     * @param string[] $tables Table name array
     */
    public function addTables(array $tables)
    {
        $this->setTables(array_merge($this->getTables(), $tables));
    }

    public function finalize() {
        $this->describe();
        $this->parseConfiguration();
    }

    /**
     * Describe and convert all the table's columns to the HTML Form
     */
    protected function describe()
    {
        foreach ($this->tables as $tableName) {
            $desc = DB::select('DESCRIBE ' . $tableName);

            foreach ($desc as $column) {
                $fieldName = $column->Field;
                $isExist = array_search($fieldName, $this->fields) !== false;

                if (!$isExist) {
                    $formTableElement = new FormTableElement();

                    // Set the form element the table name that's used to create the element
                    $formTableElement->setFromTable($tableName);

                    // Set the form element name, id based on the column name
                    $formTableElement->setName($fieldName);

                    // Set the form element data, its first stage will be null
                    $formTableElement->setData(strcmp($fieldName, 'locale_id') === 0 ? 'vi' : null);

                    // Set the form element type, the type will be map from SQL type to input HTML type
                    $formTableElement->setTableType($column->Type);

                    // Set the form element custom attribute. It is the input HTML attributes
                    $formTableElement->setAttributes([
                        'required' => strcmp($column->Null, 'NO') === 0
                    ]);

                    // Set the form element label
                    $formTableElement->setLabel(__(config('ecommerce.namespace.lang').'::product.'.$tableName.'.'.$fieldName.'.form.label'));

                    // Start the parse the form
                    $formTableElement->finalize();

                    $this->addElement($formTableElement);
                    array_push($this->fields, $fieldName);
                }
            }
        }
    }

    /**
     * Parse the custom configuration
     */
    protected function parseConfiguration()
    {
        $exclude = Arr::get($this->config, 'fields.exclude', []);

        $hidden = Arr::get($this->config, 'fields.hidden', []);

        $forceType = Arr::get($this->config, 'fields.force_type', []);
        $forceTypeKey = array_keys($forceType);

        foreach ($this->elements as $idx => $element) {

            $elementName = $element->getName();
            $elementForceTypeKey = $element->getFromTable().'::'.$elementName;

            // Remove all the exclude column in the table
            if (array_search($elementName, $exclude) !== false) {
                unset($this->elements[$idx]);
                continue;
            }

            // Hide all hidden field in the form
            if (array_search($elementName, $hidden) !== false) {
                $element->setType('hidden');
            }


            if (array_search($elementForceTypeKey, $forceTypeKey) !== false) {
                $element->setType($forceType[$elementForceTypeKey]);
            }
        }

        // Reindexes the element array
        $this->setElement(array_values($this->elements));
    }

    /**
     * Set the data into the form
     * @param array $formData The form data associative array
     */
    public function setFormData(array $formData)
    {
        foreach ($this->elements as $element) {
            $elementName = $element->getName();

            if (isset($formData[$elementName])) {
                $element->setData($formData[$elementName]);
            }
        }
    }
}
