<?php

namespace Tecpro\Core\Scripts\Support\Form;

/**
 * Form object only contains its elements, data
 */
class Form
{
    /**
     * @var string The form action
     */
    protected string $action;

    /**
     * @var string The form method
     */
    protected string $method;

    /**
     * @var FormElement[] The form elements
     */
    protected array $elements;

    /**
     * @var string[] The array of table field name
     */
    protected array $fields;

    /**
     * Initialize the form
     */
    public function __construct()
    {
        $this->action = '';
        $this->method = '';
        $this->elements = [];
        $this->fields = [];
    }

    /**
     * Get the action URL
     * @return string the action URL
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set the action URl
     * @param string $action The action URL
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     * Get the HTTP method
     * @return string the HTTP method
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set the HTTP method
     * @param string $method The HTTP method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * Get the form elements array
     * @return array The form elements array
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Set the element array to the form
     * @param FormElement[] $elements The form element array
     */
    protected function setElement(array $elements) {
        $this->elements = $elements;
    }

    /**
     * Append the element into the form
     * @param FormElement $formElement The form element
     */
    protected function addElement(FormElement $formElement)
    {
        array_push($this->elements, $formElement);
    }

    /**
     * Get the form fields name array
     * @return array The form fields name array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Render all element in the form
     * @return string The rendered form HTML
     */
    public function renderFields()
    {
        $html = '';

        foreach ($this->elements as $element) {
            $html .= $element->renderHTML();
        }

        return $html;
    }

    /**
     * Render the form
     * @return string The rendered form HTML 
     */
    public function renderForm()
    {
    }
}
