<?php

namespace Tecpro\Core\Scripts\Support\Form;

use Error;
use Illuminate\Support\Str;

class FormElement
{
    /**
     * @var string Form element name
     */
    protected string $name;

    /**
     * @var string Form element type
     */
    protected string $type;

    /**
     * @var ?string The element data
     */
    protected ?string $data;

    /**
     * @var array Form element attributes attribute
     */
    protected array $attributes;

    /**
     * @var string The form element label
     */
    protected string $label;

    /**
     * Return the element name
     * @return string The element name
     */
    public function getName()
    {
        return $this->name;
    }

    public function setName(string $fieldName)
    {
        $this->name = $fieldName;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData(?string $data)
    {
        $this->data = $data;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    protected function renderText()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>">
        </div>
    <?php
    }

    protected function renderHidden()
    {
    ?>
        <div class="mb-3 d-none">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>">
        </div>
    <?php
    }

    protected function renderNumber()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>">
        </div>
    <?php
    }

    protected function renderSelect()
    {
    ?>
        <p><?php _echo($this->type) ?></p>
    <?php
    }

    protected function renderCheckbox()
    {
    ?>
        <div class="mb-3">
            <label class="mb-3 form-check">
                <input class="form-check-input" type="<?php _echo($this->type) ?>" data-sql-value="<?php _echo($this->data) ?>" name="<?php _echo($this->name) ?>" <?php echo isset($this->data) && ($this->data === true || $this->data === 1 || strcmp($this->data, '1') === 0) ? 'checked' : '' ?> >
                <span class="form-check-label"><?php _echo($this->label) ?></span>
            </label>
        </div>
    <?php
    }

    protected function renderTextArea()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <textarea class="form-control" name="<?php _echo($this->name) ?>" rows="10"><?php _echo($this->data) ?></textarea>
        </div>
    <?php
    }

    protected function renderDate()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>">
        </div>
    <?php
    }

    protected function renderDateTime()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>">
        </div>
    <?php
    }

    protected function renderFile()
    {
    ?>
        <div class="mb-3">
            <label class="form-label"><?php _echo($this->label) ?></label>
            <input type="<?php _echo($this->type) ?>" class="form-control" name="<?php _echo($this->name) ?>" id="<?php _echo($this->name) ?>" value="<?php _echo($this->data) ?>" accept="image/jpg, image/png, image/jpeg">
        </div>
    <?php
    }

    /**
     * Render the input HTML
     * @return string The rendered HTML
     */
    public function renderHTML()
    {
        ob_start();
        switch ($this->type) {
            case 'text':
                $this->renderText();
                break;

            case 'hidden':
                $this->renderHidden();
                break;

            case 'number':
                $this->renderNumber();
                break;

            case 'textarea':
                $this->renderTextArea();
                break;

            case 'select':
                $this->renderSelect();
                break;

            case 'checkbox':
                $this->renderCheckbox();
                break;

            case 'date':
                $this->renderDate();
                break;

            case 'datetime-local':
                $this->renderDateTime();
                break;

            case 'file':
                $this->renderFile();
                break;

            default:
                throw new Error('Form element "' . $this->name . '" can not parse its type');
                break;
        }
        $html = ob_get_clean();

        return $html ? $html : '';
    }
}
