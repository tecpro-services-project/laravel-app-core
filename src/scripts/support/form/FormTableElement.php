<?php

namespace Tecpro\Core\Scripts\Support\Form;

class FormTableElement extends FormElement
{

    /**
     * @var string $fromTable The table name that's used to create the element
     */
    protected string $fromTable;

    /**
     * @var string $tableType the SQL table type
     */
    protected string $tableType;

    /**
     * Get the table name that's used to create the element
     * @return string The table name that's used to create the element
     */
    public function getFromTable() {
        return $this->fromTable;
    }

    /**
     * Set the table name that's used to create the element
     * @param string $fromTable The table name that's used to create the element
     */
    public function setFromTable(string $fromTable) {
        $this->fromTable = $fromTable;
    }

    /**
     * Get the SQL table type
     * @return string The SQL table type
     */
    public function getTableType() {
        return $this->tableType;
    }

    /**
     * Set the SQL table type
     * @param string $tableType The SQL table type
     */
    public function setTableType(string $tableType) {
        $this->tableType = $tableType;
    }

    /**
     * Parse the table type to the HTML input type
     * @param string $pattern the table pattern
     * @param string $tableType The table type
     * @param string $htmlType The expected HTML type if the table type matches the provided pattern
     */
    public function convertType(string $pattern, string $tableType, string $htmlType) {
        preg_match($pattern, $tableType, $typeMatch);

        if (count($typeMatch) !== 0) {
            $this->setType($htmlType);
        }
    }

    /**
     * Finalize the form element after setting all configuration from table or manual
     */
    public function finalize() {
        $this->convertType('/(varchar)/', $this->getTableType(), 'text');
        $this->convertType('/(text)/', $this->getTableType(), 'textarea');
        $this->convertType('/(int)/', $this->getTableType(), 'number');
        $this->convertType('/(tinyint)/', $this->getTableType(), 'checkbox');
        $this->convertType('/(date)/', $this->getTableType(), 'date');
        $this->convertType('/(timestamp)/', $this->getTableType(), 'datetime-local');
    }
}
