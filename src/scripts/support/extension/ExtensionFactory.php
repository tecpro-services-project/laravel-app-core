<?php

namespace Tecpro\Core\Scripts\Support\Extension;

use Error;
use Illuminate\Support\Arr;
use Kfriars\ArrayToFile\Facades\ArrayWriter;

class ExtensionFactory
{

    /**
     * This will should be moved in config file or database in future
     * @var array $config The extension configuration
     */
    public static $config = [
        'extend' => [
            'order' => [
                'namespaces' => [
                    'Tecpro\\Core\\',
                    'Tecpro\\CMS\\',
                    'Tecpro\\Ecommerce\\',
                    'Tecpro\\Course\\',
                ],
            ],
            'models' => [
                'product' => 'App\\Models\\Product',
                'productCategory' => 'App\\Models\\ProductCategory',
                'productCategoryItem' => 'App\\Models\\ProductCategoryItem',
            ]
        ]
    ];

    /**
     * Get the extendable object based on the provided key that's exported to the config/extend.php
     * @param string $objectKey The object key
     * @param bool $strict Set true to strictly check whether the key exists in the configution
     * @return string The namespace of the final object in the order
     */
    public static function get(string $objectKey, bool $strict = false)
    {
        $finalClassName = config("extend.$objectKey");

        if (!isset($finalClassName) && $strict) {
            throw new Error('Extend object does not exist in configuration');
        }

        return $finalClassName;
    }

    /**
     * Export final configuration results after parsing self::$config
     * @return array The final configuration result array
     */
    public static function exportFinalConfiguration() {
        $models = Arr::get(self::$config, 'extend.models', []);
        $namespaces = Arr::get(self::$config, 'extend.order.namespaces', []);
        $finalConfiguration = [];

        foreach ($models as $key => $modelNamespace) {
            foreach ($namespaces as $namespace) {
                $modelFullNamespace = '\\' . $namespace . $modelNamespace;
    
                if (class_exists($modelFullNamespace)) {
                    Arr::set($finalConfiguration, 'models.'.$key, $modelFullNamespace);
                }
            }
        }

        ArrayWriter::toFile($finalConfiguration, config_path('extend.php'));

        return $finalConfiguration;
    }
}
