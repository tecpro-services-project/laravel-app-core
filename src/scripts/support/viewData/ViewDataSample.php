<?php

namespace Tecpro\Core\Scripts\Support\ViewData;

#{{useViewData}}

class ViewDataSample extends ViewData {
    /**
     * Return the view data name that can be access in blade template with {{ $viewDataName }}
     * 
     * @return string The view data name
     */
    public function getName() {
        return 'viewDataName';
    }

    /**
     * Return the view data array binded with the view name
     * 
     * @return array The view data array binded with the view name
     */
    public function default() {
        return [
            /*
            |--------------------------------------------------------------------------
            | Custom default view data will be placed in this array
            |--------------------------------------------------------------------------
            */
        ];
    }

    /**
     * Return the authenticated view data array binded with the view name
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $authenticated Auth::user()
     * @return array The authenticated view data array binded with the view name
     */
    public function authenticated($authenticated) {
        return [
            /*
            |--------------------------------------------------------------------------
            | Custom authenticated user's view data will be placed in this array
            |--------------------------------------------------------------------------
            */
        ];
    }
}