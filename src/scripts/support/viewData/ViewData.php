<?php

namespace Tecpro\Core\Scripts\Support\ViewData;

abstract class ViewData {
    /**
     * @var array The private view data
     */
    private $viewData;

    /**
     * Return the view data name that can be access in blade template with {{ $viewDataName }}
     * 
     * @return string The view data name
     */
    abstract public function getName();

    /**
     * Return the default view data array binded with the view name
     * 
     * @return array The default view data array binded with the view name
     */
    abstract public function default();

    /**
     * Return the view data array binded with the view name when authenticated
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable|null $authenticated
     * @return array The view data array binded with the view name when authenticated
     */
    abstract public function authenticated($authenticated);

    /**
     * Set the authenticated user
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable|null $authUser authenticated user
     * @return void
     */
    public function setAuthUser($authUser) {
        $this->authUser = $authUser;
    }

    /**
     * Return the view data array binded with the view name
     * 
     * @return array The view data array binded with the view name
     */
    public function dump() {
        $this->viewData = array_merge($this->default(), isset($this->authUser) ? $this->authenticated($this->authUser) : []);
        return $this->viewData;
    }
}
